package learning.ocdjws.webservice;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
@WebService(name = "WeatherServicePort", targetNamespace = "http://www.example.org/WeatherService/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public class WeatherServicePortImpl implements WeatherServicePort {
@Override
@WebMethod(operationName = "GetCurrentTemperature", action = "http://www.example.org/WeatherService/GetCurrentTemperature")
@WebResult(name = "GetCurrentTemperatureRs", targetNamespace = "http://www.example.org/WeatherService/", partName = "parameters")
public GetCurrentTemperatureRs getCurrentTemperature(
GetCurrentTemperatureRq request) {
System.out.println(request.getCity());
ObjectFactory objectfactory = new ObjectFactory();
GetCurrentTemperatureRs response = objectfactory
.createGetCurrentTemperatureRs();
response.setTemperature(Math.random() * 100 + "Degree Celcius");
return response;
}
}
