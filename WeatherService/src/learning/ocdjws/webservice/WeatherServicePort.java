package learning.ocdjws.webservice;

public interface WeatherServicePort {

	GetCurrentTemperatureRs getCurrentTemperature(GetCurrentTemperatureRq request);

}
